﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FocalAgent.D365.Models;
/*
 * Author : Sam Jones
 * Created On : 24/05/2021
 * Purpose : 
 * Local testing of the PBAccountReparent mechanism. 
 * Created a SQLLite database to mimic the expected fields from AA SQL
 */

namespace FocalAgent.D365.Console.PurpleBricksAccountReparent
{
    public class SQLLiteTestDatabase
    {
        private static SQLiteConnection CreateSQLiteConnection()
        {
            SQLiteConnection ret = null;
            try
            {
                ret = new SQLiteConnection("Data Source=c:\\sqlite\\DataIntegrityTest.db;Version=3;New=True;Compress=True;");
                ret.Open();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("There was an error in the SQLiteConnection : " + ex.Message);
            }

            return ret;
        }

        public static List<AAContactModel> retrieveContactsFromTestDataSource()
        {
            List<AAContactModel> ret = new List<AAContactModel>();

            try
            {
                SQLiteConnection conn = CreateSQLiteConnection();

                if (conn != null)
                {
                    SQLiteDataAdapter sqlDataAdapt = new SQLiteDataAdapter("SELECT contactid,firstname,lastname,parentcustomerid,suffix,fc_aacustomerid,name,businesstypecode FROM ContactToAccount", conn);
                    DataSet ds = new DataSet();
                    sqlDataAdapt.Fill(ds, "ContactToAccount");
                    if (ds != null)
                    {
                        return ds.Tables[0].AsEnumerable()
                            .Select(d =>
                                new AAContactModel()
                                {
                                    contactId = d.Field<string>("contactid"),
                                    firstName = d.Field<string>("firstname"),
                                    lastName = d.Field<string>("lastname"),
                                    parentCustomerId = d.Field<string>("parentcustomerid"),
                                    suffix = d.Field<string>("suffix"),
                                    aaCustomerId = d.Field<string>("fc_aacustomerid"),
                                    name = d.Field<string>("name"),
                                    businessTypeCode = d.Field<string>("businesstypecode")

                                }).ToList();
                    }
                    conn.Close();
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }
    }

}
