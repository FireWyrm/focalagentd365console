﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using FocalAgent.D365.Helpers;
using Microsoft.Xrm.Sdk;
using NLog;
using Microsoft.Xrm.Sdk.Query;

//SQLite Connection
using System.Data.SQLite;
using System.Net.Http;
using FocalAgent.D365.Models;

/*
 * Author : Sam Jones
 * Requestor : Neil Coombs
 * Created On : Monday 24/05/2021
 * Type : Console App - One Time
 * Purpose : To reparent Purple Bricks contacts to a newly created Account and then to reparent that Account to the overall Purple Bricks account
 * 
 * Adjunct Project : FocalAgent.D365.Helpers
 * Helpers to create the CRM Service Proxy connection, update entities, query entities and manipulate data in a generic way.
 * 
 * Input Parameters : NONE
 * 
 * Deployment Considerations : None.
 * External Libraries : NLog for logging (see bin directory of the Project for logs)
 */

namespace FocalAgent.D365.Console.PurpleBricksAccountReparent
{
    public class PBAccountReparent
    {
        public static IOrganizationService _service; //Support for the Service

        //******************************* SETTINGS ***********************************************
        public static string _connType = "Prod"; //Change this to switch between ConnectionStrings. Acceptable values : Dev, Stage, Prod
        //****************************************************************************************

        public static CRMOperations _crmOperations;
        public static CRMQuery _crmQuery;


        public static Logger localNLog = LogManager.GetLogger("localLog");
        public static Logger outcomeLog = LogManager.GetLogger("outcomeLog");
        static void Main(string[] args)
        {
            connectCRM();

            if (_service != null)
            {
                //QueryEngine
                _crmQuery = new CRMQuery(_service, localNLog);
                //Operations Engine
                _crmOperations = new CRMOperations(_service, localNLog);

                //Connect to AA SQL database : Connection string
                Services.ADO.SQLDataConnector.ConnectionString = "GlobalAAConnection";

                System.Console.WriteLine("Congratulations, you are connected to CRM : " + _connType);
                CRMService.WriteLog("-------------Connected to CRM--------------", false, true, localNLog);

                List<AAContactModel> contactsToProcess = new List<AAContactModel>();
                if (_connType == "Dev")
                {
                    //Connect to AA SQL database and retrieve the basic data to process
                    contactsToProcess = SQLLiteTestDatabase.retrieveContactsFromTestDataSource();
                }
                else
                {
                    string testVictimId = "d19ef689-0412-ea11-a811-00224801c499";
                    contactsToProcess = retrieveContactsFromAADataSource(testVictimId);
                }

                if (contactsToProcess.Count > 0)
                {
                    CRMService.WriteLog("Success. Found " + contactsToProcess.Count + " contacts to process", true, true,localNLog);

                    List<CRMContactModel> listCRMContactData = new List<CRMContactModel>();
                    foreach(AAContactModel cont in contactsToProcess)
                    {
                        //CRMContactModel foundContactData = getCRMContactData(cont);
                        //CRMService.WriteLog("Locating CRM Data for " + cont.firstName + " " +cont.lastName, true, true, localNLog);
                        //if (foundContactData != null)
                        //{
                        //    listCRMContactData.Add(foundContactData);
                        //}
                    }

                    if (listCRMContactData.Count > 0)
                    {
                        CRMService.WriteLog("CRM Contact data located. Processing " + listCRMContactData.Count, true, true, localNLog);

                        int countCompleted = 0;
                        foreach (CRMContactModel cont in listCRMContactData)
                        {
                           //bool completed = createCRMAccount(cont);

                            //if (completed)
                            //{
                            //    countCompleted++;
                            //}
                        }

                        CRMService.WriteLog("Completed processing : " + countCompleted + " records", true, true, localNLog);
                    }
                }

                CRMService.WriteLog("All processing completed : Goodbye", true, true, localNLog);

            }
        }


        static void connectCRM()
        {
            string connection = string.Empty;

            switch (_connType)
            {
                case "Dev":
                    connection = ConfigurationManager.ConnectionStrings["DevCrmConnection"].ConnectionString;
                    break;
                case "Stage":
                    connection = ConfigurationManager.ConnectionStrings["StagingConnection"].ConnectionString;
                    break;
                case "Prod":
                    connection = ConfigurationManager.ConnectionStrings["ProductionCrmConnection"].ConnectionString;
                    break;
            }

            if (connection != string.Empty)
            {
                //go ahead and create the service
                _service = CRMService.ConnectToCRM(_connType, connection, localNLog);
            }
        }

        private static List<AAContactModel> retrieveContactsFromAADataSource(string testVictim = null)
        {
            List<AAContactModel> ret = new List<AAContactModel>();

            try
            {
                if (testVictim != null)
                {
                    ret = Services.AccountContactService.getPBContactsFromAA(testVictim);
                }
                else
                {
                    ret = Services.AccountContactService.getPBContactsFromAA();
                }
                
            }
            catch (Exception ex)
            {
                localNLog.Fatal("Error in PBAccountReparent.retrieveContactsFromAA : " + ex.Message);
            }

            return ret;
        }

        private static CRMContactModel getCRMContactData(AAContactModel cont)
        {
            CRMContactModel ret = new CRMContactModel();
            try
            {
                //retrieve existing contact from CRM by contactId
                Guid contactId = new Guid(cont.contactId);
                Entity existingContact = _crmQuery.retrieveEntity("contact", contactId, new ColumnSet(true));

                if (existingContact != null)
                {
                    #region contact basic info
                    ret.contactId = existingContact.Id;

                    if (existingContact.Attributes.Contains("parentcustomerid"))
                    {
                        ret.parentCustomerId = existingContact.GetAttributeValue<EntityReference>("parentcustomerid").Id;
                    }

                    if (existingContact.Attributes.Contains("firstname"))
                    {
                        ret.firstName = existingContact.GetAttributeValue<string>("firstname");
                    }

                    if (existingContact.Attributes.Contains("lastname"))
                    {
                        ret.lastName = existingContact.GetAttributeValue<string>("lastname");
                    }

                    if (existingContact.Attributes.Contains("mobilephone"))
                    {
                        ret.mobilePhone = existingContact.GetAttributeValue<string>("mobilephone");
                    }

                    if (existingContact.Attributes.Contains("telephone1"))
                    {
                        ret.businessPhone = existingContact.GetAttributeValue<string>("telephone1");
                    }

                    if (existingContact.Attributes.Contains("emailaddress1"))
                    {
                        ret.emailAddress = existingContact.GetAttributeValue<string>("emailaddress1");
                    }

                    ret.AAId = cont.suffix;
                    #endregion

                    #region contact address
                    CRMAddressModel address = new CRMAddressModel();

                    if (existingContact.Attributes.Contains("address1_line1"))
                    {
                        address.Address_1 = existingContact.GetAttributeValue<string>("address1_line1");
                    }

                    if (existingContact.Attributes.Contains("address1_line2"))
                    {
                        address.Address_2 = existingContact.GetAttributeValue<string>("address1_line2");
                    }

                    if (existingContact.Attributes.Contains("address1_line3"))
                    {
                        address.Address_3 = existingContact.GetAttributeValue<string>("address1_line3");
                    }

                    if (existingContact.Attributes.Contains("address1_city"))
                    {
                        address.Address_City = existingContact.GetAttributeValue<string>("address1_city");
                    }

                    if (existingContact.Attributes.Contains("address1_stateorprovince"))
                    {
                        address.Address_County = existingContact.GetAttributeValue<string>("address1_stateorprovince");
                    }

                    if (existingContact.Attributes.Contains("address1_postalcode"))
                    {
                        address.Address_Postcode = existingContact.GetAttributeValue<string>("address1_postalcode");
                    }

                    ret.crmAddress = address;
                    #endregion 
                }
                else
                {
                    CRMService.WriteLog("Unable to retrieve CRMContact for : " + cont.contactId, true, true, localNLog);
                }
            }
            catch (Exception ex)
            {
                CRMService.WriteLog("Error in PBAccountReparent.reparentContact : " + ex.Message, true, true, localNLog);
            }

            return ret;
        }

        private static bool createCRMAccount(CRMContactModel oldContact)
        {
            bool ret = false;
            try
            {
                CRMService.WriteLog("Creating Account for : " + oldContact.contactId,true,true,localNLog);
                Entity newAccount = new Entity("account");

                newAccount["name"] = "Purple Bricks Agency - " + oldContact.firstName + " " + oldContact.lastName;
                newAccount["parentaccountid"] = new EntityReference("account", oldContact.parentCustomerId);
                newAccount["telephone1"] = oldContact.businessPhone;
                newAccount["telephone2"] = oldContact.mobilePhone;
                newAccount["emailaddress1"] = oldContact.emailAddress;
                newAccount["customertypecode"] = new OptionSetValue(3); //Customer
                newAccount["industrycode"] = new OptionSetValue(4); // Estate Agents
                newAccount["businesstypecode"] = new OptionSetValue(948150003); //Branch
                newAccount["fc_aacustomerid"] = oldContact.AAId;

                if (oldContact.crmAddress != null)
                {
                    newAccount["address1_line1"] = oldContact.crmAddress.Address_1;
                    newAccount["address1_line2"] = oldContact.crmAddress.Address_2;
                    newAccount["address1_line3"] = oldContact.crmAddress.Address_3;
                    newAccount["address1_city"] = oldContact.crmAddress.Address_City;
                    newAccount["address1_county"] = oldContact.crmAddress.Address_County;
                    newAccount["address1_postalcode"] = oldContact.crmAddress.Address_Postcode;
                   
                }

                Guid newAccountId = _crmOperations.createEntity(newAccount);

                if (newAccountId != Guid.Empty)
                {
                    //now update the oldContact with a parentcustomerid of the new account
                    Entity theContact = _crmQuery.retrieveEntity("contact", oldContact.contactId, new ColumnSet(new string[] { "parentcustomerid" }));

                    if (theContact != null)
                    {
                        theContact["parentcustomerid"] = new EntityReference("account", newAccountId);
                        _crmOperations.updateEntity(theContact);
                        CRMService.WriteLog("Contact reparented to Account : " + newAccountId.ToString(), true, true, localNLog);
                        ret = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CRMService.WriteLog("Error in PBAccountReparent.createCRMAccount : " + ex.Message,true,true,localNLog);
            }

            return ret;
        }
    }
}
