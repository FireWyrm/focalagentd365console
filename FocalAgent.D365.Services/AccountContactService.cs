﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FocalAgent.D365.Models;

namespace FocalAgent.D365.Services
{
    public static class AccountContactService
    {
        public static List<AAContactModel> getPBContactsFromAA(string testVictimId = null)
        {
            List<AAContactModel> ret = null;

            try
            {
                SqlParameter testVictim = null;
                if (testVictimId != string.Empty)
                {
                    testVictim = ADO.SQLDataConnector.CreateParameter("@contactid",SqlDbType.VarChar,testVictimId);
                }
                SqlParameter sqlReturnValue = ADO.SQLDataConnector.CreateReturnParameter();
                DataSet dataSet = null;
                if (testVictimId != null)
                {
                    dataSet = ADO.SQLDataConnector.ExecuteDataSet("[dbo].[usp_GetContactToAccount]", testVictim, sqlReturnValue);
                }
                else
                {
                    dataSet = ADO.SQLDataConnector.ExecuteDataSet("[dbo].[usp_GetContactToAccount]", sqlReturnValue);
                }
                

                if (dataSet != null)
                {
                    return dataSet.Tables[0].AsEnumerable()
                        .Select(d =>
                            new AAContactModel()
                            {
                                contactId = d.Field<string>("contactid"),
                                firstName = d.Field<string>("firstname"),
                                lastName = d.Field<string>("lastname"),
                                parentCustomerId = d.Field<string>("parentcustomerid"),
                                suffix = d.Field<string>("suffix"),
                                aaCustomerId = d.Field<string>("fc_aacustomerid"),
                                name = d.Field<string>("name"),
                                businessTypeCode = d.Field<string>("businesstypecode")

                            }).ToList();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }

    }
}
