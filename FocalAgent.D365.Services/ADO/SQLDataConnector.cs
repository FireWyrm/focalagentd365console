﻿using System;
using System.Collections.Generic;

using System.Text;

using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;

namespace FocalAgent.D365.Services.ADO
{
    public static class SQLDataConnector
    {
        /// <summary>
        /// The connection string.
        /// </summary>
        private static ConnectionStringSettings connectionString;

        /// <summary>
        /// The command timeout.
        /// </summary>
        private static int commandTimeout = 30;

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public static string ConnectionString
        {
            get
            {
                return connectionString?.Name;
            }

            set
            {
                connectionString = ConfigurationManager.ConnectionStrings[value];
            }
        }

        /// <summary>
        /// Gets or sets the command timeout.
        /// </summary>
        /// <value>
        /// The command timeout.
        /// </value>
        /// <exception cref="System.Exception">ADO Command Timeout is invalid.</exception>
        public static int CommandTimeout
        {
            get
            {
                return commandTimeout;
            }

            set
            {
                if (value < 0)
                {
                    throw new Exception("ADO Command Timeout is invalid.");
                }

                commandTimeout = value;
            }
        }

        /// <summary>
        /// Executes the data set.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The data returned by the stored procedure.
        /// </returns>
        public static DataSet ExecuteDataSet(string storedProcedureName, params SqlParameter[] parameters)
        {
            return ExecuteDataSet(null, storedProcedureName, string.Empty, parameters);
        }

        /// <summary>
        /// Executes the data set.
        /// </summary>
        /// <param name="dataSet">The data set.</param>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The data returned by the stored procedure.
        /// </returns>
        public static DataSet ExecuteDataSet(DataSet dataSet, string storedProcedureName, params SqlParameter[] parameters)
        {
            return ExecuteDataSet(dataSet, storedProcedureName, string.Empty, parameters);
        }

        /// <summary>
        /// Executes the data set.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The data returned by the stored procedure.
        /// </returns>
        public static DataSet ExecuteDataSet(string storedProcedureName, string tableName, params SqlParameter[] parameters)
        {
            return ExecuteDataSet(null, storedProcedureName, tableName, parameters);
        }

        /// <summary>
        /// Executes the data set.
        /// </summary>
        /// <param name="dataSet">The data set.</param>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The data returned by the stored procedure.
        /// </returns>
        /// <exception cref="System.Exception">An error occurred while populating a dataset in DataAccessHelper.ExecuteDataSet().  + ex.Message.</exception>
        public static DataSet ExecuteDataSet(DataSet dataSet, string storedProcedureName, string tableName, params SqlParameter[] parameters)
        {
            if (dataSet == null)
            {
                dataSet = new DataSet();
            }

            DbCommand command = null;
            DbDataAdapter adapter = null;

            try
            {
                command = CreateCommand(storedProcedureName, parameters);
                adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;

                if (string.IsNullOrEmpty(tableName))
                {
                    adapter.Fill(dataSet);
                }
                else
                {
                    adapter.Fill(dataSet, tableName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while populating a dataset in DataAccessHelper.ExecuteDataSet(). " + ex.Message, ex);
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        if (command.Connection.State != ConnectionState.Closed)
                        {
                            command.Connection.Close();
                        }

                        command.Connection.Dispose();
                    }

                    command.Dispose();
                }

                if (adapter != null)
                {
                    adapter.Dispose();
                }
            }

            return dataSet;
        }

        /// <summary>
        /// Executes the data table.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The data returned by the stored procedure.
        /// </returns>
        /// <exception cref="System.Exception">No data returned from query:  + storedProcedureName.</exception>
        public static DataTable ExecuteDataTable(string storedProcedureName, params SqlParameter[] parameters)
        {
            DataSet ds = ExecuteDataSet(storedProcedureName, parameters);

            if (ds.Tables.Count == 0)
            {
                throw new Exception("No data returned from query: " + storedProcedureName);
            }

            return ds.Tables[0];
        }

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <exception cref="System.Exception">An error occurred while executing a non query in DataAccessHelper.ExecuteNonQuery().  + ex.Message.</exception>
        public static void ExecuteNonQuery(string storedProcedureName, params SqlParameter[] parameters)
        {
            DbCommand command = null;

            try
            {
                command = CreateCommand(storedProcedureName, parameters);

                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }

                command.ExecuteNonQuery();
                command.Connection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while executing a non query in DataAccessHelper.ExecuteNonQuery(). " + ex.Message, ex);
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        if (command.Connection.State != ConnectionState.Closed)
                        {
                            command.Connection.Close();
                        }

                        command.Connection.Dispose();
                    }

                    command.Dispose();
                }
            }
        }

        /// <summary>
        /// Creates the parameter.
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="sqlDbType">Type of the SQL database.</param>
        /// <param name="parameterValue">The parameter value.</param>
        /// <returns>
        /// The new SQL Parameter.
        /// </returns>
        public static SqlParameter CreateParameter(string parameterName, SqlDbType sqlDbType, object parameterValue)
        {
            return CreateParameter(parameterName, sqlDbType, parameterValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter.
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="sqlDbType">Type of the SQL database.</param>
        /// <param name="parameterValue">The parameter value.</param>
        /// <param name="direction">The direction.</param>
        /// <returns>
        /// The new SQL Parameter.
        /// </returns>
        public static SqlParameter CreateParameter(string parameterName, SqlDbType sqlDbType, object parameterValue, ParameterDirection direction)
        {
            SqlParameter parameter = new SqlParameter(parameterName, sqlDbType);
            parameter.Direction = direction;
            SetParameterValue(parameter, parameterValue);
            return parameter;
        }

        /// <summary>
        /// Creates the return parameter.
        /// </summary>
        /// <returns>
        /// Returns a SQL Parameter.
        /// </returns>
        public static SqlParameter CreateReturnParameter()
        {
            SqlParameter parameter = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
            parameter.Direction = ParameterDirection.ReturnValue;
            return parameter;
        }

        /// <summary>
        /// Creates the command.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// Returns a DB Command.
        /// </returns>
        private static DbCommand CreateCommand(string storedProcedureName, SqlParameter[] parameters)
        {
            DbConnection connection = CreateConnection();
            DbCommand command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            command.CommandTimeout = CommandTimeout;
            AddParameters(parameters, command);
            return command;
        }

        /// <summary>
        /// Adds the parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="command">The command.</param>
        private static void AddParameters(SqlParameter[] parameters, DbCommand command)
        {
            foreach (SqlParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
        }

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <param name="parameterValue">The parameter value.</param>
        private static void SetParameterValue(SqlParameter parameter, object parameterValue)
        {
            if (parameterValue == null)
            {
                parameter.IsNullable = true;
                parameterValue = DBNull.Value;
            }
            else if (parameter.DbType == DbType.DateTime)
            {
                parameterValue = Convert.ToDateTime(parameterValue);
            }

            parameter.Value = parameterValue;
        }

        /// <summary>
        /// Creates the connection.
        /// </summary>
        /// <returns>
        /// The open database connection.
        /// </returns>
        /// <exception cref="System.Exception">ADO Connection String not set.</exception>
        private static DbConnection CreateConnection()
        {
            if (connectionString == null)
            {
                throw new Exception("ADO Connection String not set.");
            }

            DbConnection connection = DbProviderFactories.GetFactory(connectionString.ProviderName).CreateConnection();
            connection.ConnectionString = connectionString.ConnectionString;
            connection.Open();
            return connection;
        }
    }
}
