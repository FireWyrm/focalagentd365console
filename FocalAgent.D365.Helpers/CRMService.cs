﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;


namespace FocalAgent.D365.Helpers
{    
    public class CRMService
    {
        public static IOrganizationService _service;
        public static Logger _serviceLog;

        public CRMService(Logger logFile)
        {
            _serviceLog = logFile;
        }
        public static IOrganizationService ConnectToCRM(string connectionType, string connectionString,Logger logFile)
        {
            IOrganizationService ret = null;
            CrmServiceClient conn = null;
            _serviceLog = logFile;

            // Remember to change this ConnectionString based on the Environment in the App.Config
            if (connectionString != string.Empty)
            {
                conn = new CrmServiceClient(connectionString);
            }
            
            _serviceLog.Info("Seeking Connection Type : " + connectionType);
            _serviceLog.Info("Trying to initialise OrganizationServiceProxy");


            try
            {
                if (conn != null && conn.IsReady)
                {
                    _serviceLog.Info("Connected Org Friendly Name : " + conn.ConnectedOrgFriendlyName);
                    _serviceLog.Info("CrmConnectOrgUriActual :" + conn.CrmConnectOrgUriActual);
                    _serviceLog.Info("Service Is Ready :  " + conn.IsReady);

                    _serviceLog.Info("Connection is ready : initiating Service Client");
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    _service = (IOrganizationService)conn.OrganizationWebProxyClient != null ? (IOrganizationService)conn.OrganizationWebProxyClient : (IOrganizationService)conn.OrganizationServiceProxy;

                    if (_service != null)
                    {
                        _serviceLog.Info("Seeking CRM Version");
                        RetrieveVersionRequest versionRequest = new RetrieveVersionRequest();
                        RetrieveVersionResponse versionResponse = (RetrieveVersionResponse)_service.Execute(versionRequest);
                        _serviceLog.Info("Found CRM Version {0}", versionResponse.Version);

                        // Test Call
                        Guid whoAmIid = ((WhoAmIResponse)_service.Execute(new WhoAmIRequest())).UserId;
                        if (whoAmIid != Guid.Empty)
                        {
                            _serviceLog.Info("Successful connection to CRM");
                            _serviceLog.Info("WhoAmI : " + whoAmIid);
                            Entity user = _service.Retrieve("systemuser", whoAmIid, new ColumnSet(true));
                            if (user != null)
                            {
                                Console.WriteLine("Successful Connection : " + conn.ConnectedOrgFriendlyName);
                                _serviceLog.Info("UserName : " + user.GetAttributeValue<string>("fullname"));
                                _serviceLog.Info("DomainName : " + user.GetAttributeValue<string>("domainname"));
                                Console.WriteLine("Welcome UserName : " + user.GetAttributeValue<string>("fullname"));

                                ret = _service;
                            }
                            else
                            {
                                _serviceLog.Fatal("Unable to get user from CRM : WhoAmI request failed");
                            }
                        }
                    }
                }
                else
                {
                    if (conn != null)
                    {
                        _serviceLog.Fatal("Last CRM Error : " + conn.LastCrmError);
                        _serviceLog.Fatal("Last CRM Exception : " + conn.LastCrmException);
                        _serviceLog.Fatal("Service was not ready for initialisation : IOrganizationService provision failed. Exiting");
                    }
                    else
                    {
                        _serviceLog.Fatal("ERROR : Connection string is empty. Cannot connect to CRM");
                    }
                    
                }
            }
            catch (FaultException<IOrganizationService> ex)
            {
                _serviceLog.Fatal(ex.Message);
                throw;
            }
            catch (CommunicationException ex)
            {
                _serviceLog.Fatal(ex);
                throw;
            }
            catch (Exception ex)
            {
                _serviceLog.Fatal(ex);
                throw;
            }

            return ret;
        }

        public static void WriteLog(string logMessage, bool toConsole, bool toLocalLog, Logger logFile = null)
        {

            if (toConsole == true)
            {
                System.Console.WriteLine(logMessage);
            }

            if (logFile != null && toLocalLog == true)
            {
                logFile.Info(logMessage);
            }

        }
    }
}
