﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using NLog;
using System;

namespace FocalAgent.D365.Helpers
{
    public class CRMOperations
    {
		IOrganizationService _service;
		Logger _operationsLog;

		public CRMOperations(IOrganizationService service, Logger logFile)
        {
			_service = service;
			_operationsLog = logFile;
        }
		
		public bool setStateRequest(string entitylogicalname, Guid entityid, int statecode, int statuscode)
		{
			bool ret = false;
			SetStateRequest setState = new SetStateRequest();
			setState.EntityMoniker = new EntityReference(entitylogicalname, entityid);
			setState.State = new OptionSetValue(statecode);
			setState.Status = new OptionSetValue(statuscode);

			try
			{
                SetStateResponse resp = (SetStateResponse)_service.Execute(setState);
                if (resp != null)
                {
                    ret = true;
                }
            }
			catch (Exception ex)
			{
				_operationsLog.Fatal("Operations : Error in Operations.SetStateRequest : " + ex.Message);
			}

			return ret;
		}

		public Entity retrieveEntity(string entityLogicalName, Guid entityId, ColumnSet cols)
		{
			Entity ret = null;
			try
			{
				ret = _service.Retrieve(entityLogicalName, entityId, cols);
			}
			catch (Exception ex)
			{
				_operationsLog.Fatal("Operations : Error in Operations.RetrieveEntity : " + ex.Message);
			}
			return ret;
		}

		public bool updateEntity(Entity e)
        {
			bool ret = false;
            try
            {
				_service.Update(e);
				ret = true;
            }
			catch (Exception ex)
            {
				_operationsLog.Fatal("Operations : Error in Operations.UpdateEntity : " + ex.Message);
			}
			return ret;
        }

		public Guid createEntity(Entity e)
        {
			Guid ret = Guid.Empty;
            try
            {
				ret = _service.Create(e);
            }
			catch(Exception ex)
            {
				_operationsLog.Fatal("Operations : Error in Operations.createEntity : " + ex.Message);
            }

			return ret;

        }

		public bool executeWorkflow(Guid workflowid, Guid entityid)
		{
			bool ret = true;

			ExecuteWorkflowRequest request = new ExecuteWorkflowRequest()
			{
				WorkflowId = workflowid,
				EntityId = entityid
			};

            try
            {
				ExecuteWorkflowResponse response = (ExecuteWorkflowResponse)_service.Execute(request);
			}
			catch (Exception ex)
            {
				_operationsLog.Fatal("Operations : Error in Operations.ExecuteWorkflow : " + ex.Message);
            }

			return ret;
		}
	}
}
