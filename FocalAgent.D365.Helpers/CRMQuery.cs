﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FocalAgent.D365.Helpers
{
    public class CRMQuery
    {
        IOrganizationService _service;
        Logger _queryLog;
        public CRMQuery(IOrganizationService service, Logger logFile)
        {
            _service = service;
            _queryLog = logFile;
        }

        public Entity retrieveEntity(string entityLogicalName, Guid entityId, ColumnSet cols)
        {
            Entity ret = null;
            try
            {
                ret = _service.Retrieve(entityLogicalName, entityId, cols);
            }
            catch (Exception ex)
            {
                _queryLog.Fatal("Unable to retrieve entity : " + ex.Message.ToString());
            }
            return ret;
        }

		public EntityCollection generalQuery(string entityLogicalName, ColumnSet cols)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = cols;

			//active only
			qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));

			try
			{
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}

		public EntityCollection generalQuery(string entityLogicalName, ColumnSet cols, List<ConditionExpression> lstConditions)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = cols;
			foreach (ConditionExpression condEx in lstConditions)
			{
				qe.Criteria.AddCondition(condEx);
			}
			qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));

			try
			{
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}

		public EntityCollection generalQuery(string entityLogicalName, ColumnSet cols, List<ConditionExpression> lstConditions, Int32 statecode)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = cols;
			foreach (ConditionExpression condEx in lstConditions)
			{
				qe.Criteria.AddCondition(condEx);
			}
			qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, statecode));

			try
			{
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}

		public EntityCollection generalQuery(string entityLogicalName, ColumnSet cols, List<ConditionExpression> lstConditions, bool includeState)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = cols;
			foreach (ConditionExpression condEx in lstConditions)
			{
				qe.Criteria.AddCondition(condEx);
			}
			if (includeState == true)
			{
				qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
			}

			try
			{
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}

		public EntityCollection generalQuery(string entityLogicalName, ColumnSet cols, List<ConditionExpression> lstConditions, OrderExpression order, bool takeTop1)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = cols;
			foreach (ConditionExpression condEx in lstConditions)
			{
				qe.Criteria.AddCondition(condEx);
			}
			qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
			qe.Orders.Add(order);

			if (takeTop1)
			{
				PagingInfo pageInfo = new PagingInfo();
				pageInfo.Count = 1;
				pageInfo.PageNumber = 1;

				qe.PageInfo = pageInfo;
			}

			try
			{
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}


		public EntityCollection generalQuery(string entityLogicalName, bool allCols, List<ConditionExpression> lstConditions, OrderExpression order, bool takeTop1)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = new ColumnSet(allCols);
			foreach (ConditionExpression condEx in lstConditions)
			{
				qe.Criteria.AddCondition(condEx);
			}
			qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
			qe.Orders.Add(order);

			if (takeTop1)
			{
				PagingInfo pageInfo = new PagingInfo();
				pageInfo.Count = 1;
				pageInfo.PageNumber = 1;

				qe.PageInfo = pageInfo;
			}

			try
			{
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}

		public EntityCollection generalQuery(string entityLogicalName, bool allCols, List<ConditionExpression> lstConditions, bool includeState)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = new ColumnSet(allCols);
			foreach (ConditionExpression condEx in lstConditions)
			{
				qe.Criteria.AddCondition(condEx);
			}
			if (includeState == true)
			{
				qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
			}

			try
			{
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}

		public EntityCollection generalQuery(string entityLogicalName, bool allCols, List<ConditionExpression> lstConditions)
		{
			EntityCollection ec = new EntityCollection();
			QueryExpression qe = new QueryExpression(entityLogicalName);
			qe.ColumnSet = new ColumnSet(allCols);
			foreach (ConditionExpression condEx in lstConditions)
			{
				qe.Criteria.AddCondition(condEx);
			}
			qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));

            try
            {
				ec = _service.RetrieveMultiple(qe);
			}
			catch (Exception ex)
            {
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}

			return ec;
		}


		public EntityCollection generalQuery(string FetchXML)
		{
			EntityCollection ret = new EntityCollection();
			RetrieveMultipleRequest fetchRequest = new RetrieveMultipleRequest
			{
				Query = new FetchExpression(FetchXML)
			};

			try
			{
				ret = ((RetrieveMultipleResponse)_service.Execute(fetchRequest)).EntityCollection;
			}
			catch (Exception ex)
			{
				_queryLog.Fatal("Query : Error in GeneralQuery : " + ex.Message);
			}


			return ret;
		}

		

	}
}
