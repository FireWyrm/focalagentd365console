﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using FocalAgent.D365.Helpers;
using Microsoft.Xrm.Sdk;
using NLog;
using Microsoft.Xrm.Sdk.Query;

/*
 * Author : Sam Jones
 * Requestor : Kirsty Sowden
 * Created On : Thu 20/05/2021
 * Type : Console App - One Time
 * Purpose : To align the broken Order (salesorder) statuses that have become out of sync with reality after development of other features.
 * This class selects a set of OrderLines based on the status of the parent Order and then calculates the correct StatusCode and Billing Status of the order depending on criteria
 * 
 * Adjunct Project : FocalAgent.D365.Helpers
 * Helpers to create the CRM Service Proxy connection, update entities, query entities and manipulate data in a generic way.
 * 
 * Input Parameters : Either "N" for all Orders or an individual OrderNo (eg 1033562)
 * 
 * Deployment Considerations : None.
 * External Libraries : NLog for logging (see bin directory of the Project for logs)
 */

namespace FocalAgent.D365.Console.IssueStatus
{
    public class IssueStatus
    {
        #region Enums
        enum _billingStatus
        {
            NotBilled = 948150000,
            ReadyToBill = 948150001,
        }

        enum _orderStatus
        {
            OrderLinesFulfilled = 948150001,
        }

        #endregion

        public static IOrganizationService _service; //Support for the Service

        //******************************* SETTINGS ***********************************************
        public static string _connType = "Dev"; //Change this to switch between ConnectionStrings. Acceptable values : Dev, Stage, Prod
        //****************************************************************************************

        public static CRMOperations _crmOperations;
        public static CRMQuery _crmQuery;
        

        public static Logger localNLog = LogManager.GetLogger("localLog");
        public static Logger outcomeLog = LogManager.GetLogger("outcomeLog");
        static void Main(string[] args)
        {
            connectCRM();

            if (_service != null)
            {
                //QueryEngine
                _crmQuery = new CRMQuery(_service, localNLog);
                //Operations Engine
                _crmOperations = new CRMOperations(_service, localNLog);

                
                System.Console.WriteLine("Congratulations, you are connected to CRM : " + _connType);
                CRMService.WriteLog("-------------Connected to CRM--------------",false,true,localNLog);

                System.Console.Write("Single Order Number or ALL orders by pressing 'N' : ");
                string singleOrCont = System.Console.ReadLine();
                string singleOrContRead = singleOrCont.ToUpper();
                bool isValid = isAllDigits(singleOrCont);

                if (singleOrCont.ToUpper() == "N")
                {
                    //handle all order/orderlines
                    CRMService.WriteLog("All OrderLines",true,true,localNLog);

                    executeOperationOne();
                    executeOperationTwo();
                }
                else if (isValid)
                {
                    //single Order input
                    CRMService.WriteLog("Order Number : " + singleOrCont,true,true,localNLog);
                    executeOperationOne(singleOrCont);
                    executeOperationTwo(singleOrCont);
                }
                else
                {
                    System.Console.WriteLine("Invalid Input : Finished");
                }

                CRMService.WriteLog("Finished Execution. Goodbye", true, true, localNLog); 
            }
            else
            {
                CRMService.WriteLog("Error : Service does not exist. Not connected to CRM", true, true, localNLog);
            }
        }

        /// <summary>
        /// Connects to CRM using CRMService helper
        /// </summary>
        static void connectCRM()
        {
            string connection = string.Empty;

            switch (_connType)
            {
                case "Dev": 
                    connection = ConfigurationManager.ConnectionStrings["DevCrmConnection"].ConnectionString;
                    break;
                case "Stage":
                    connection = ConfigurationManager.ConnectionStrings["StagingConnection"].ConnectionString;
                    break;
                case "Prod":
                    connection = ConfigurationManager.ConnectionStrings["ProductionCrmConnection"].ConnectionString;
                    break;
            }

            if (connection != string.Empty)
            {
                //go ahead and create the service
                _service = CRMService.ConnectToCRM(_connType,connection,localNLog);
            }
        }

        /// <summary>
        /// Operation One : sets the Order Billing Status
        /// </summary>
        /// <param name="singleOrderNo">string representing the OrderNo input if available</param>
        static void executeOperationOne(string singleOrderNo = null)
        {
            CRMService.WriteLog("Executing Operation One ----------------->", true, true, localNLog);
            outcomeLog.Info("Executing Operation One ----------------->");

            Int32[] billingStatuses = new Int32[] { (int)_billingStatus.NotBilled, (int)_billingStatus.ReadyToBill };
            List<Entity> foundOrderLines = null;
            if (singleOrderNo != null)
            {                 
                foundOrderLines = getOrderLines(billingStatuses,singleOrderNo);
            }
            else
            {
                foundOrderLines = getOrderLines(billingStatuses);
            }

            if (foundOrderLines.Count > 0)
            {
                CRMService.WriteLog("Found : " + foundOrderLines.Count + " OrderLines to process",true,true,localNLog);

                //Process Lines : 
                //Rule : if 1 or more items are "complete" status, then the BillingStatus of the Order = Ready To Bill

                //get a list of Orders (test 980005)
                var groupOrderLinesByOrder = foundOrderLines.GroupBy(item => (Guid)item.GetAttributeValue<AliasedValue>("so.salesorderid").Value).Select(group => new { OrderId = group.Key, Items = group.ToList() }).ToList();
                CRMService.WriteLog("Found : " + groupOrderLinesByOrder.Count() + " distinct Orders to process", true, true, localNLog);

                Int32 countProcessed = 0;
                Int32 countUpdated = 0;
                foreach (var grpOrder in groupOrderLinesByOrder)
                {
                    bool updateSuccess = false; //set a false flag
                    string orderNo = "UNKNOWN";
                    if (grpOrder.Items[0].Attributes.Contains("so.name"))
                    {
                       orderNo = grpOrder.Items[0].GetAttributeValue<AliasedValue>("so.name").Value.ToString();
                    }
                        
                    Guid salesOrderId = (Guid)grpOrder.OrderId;
                    //get the OrderLines only for this Order
                    List<Entity> orderLinesByOrder = foundOrderLines.Where(a => (Guid)a.GetAttributeValue<AliasedValue>("so.salesorderid").Value == salesOrderId).ToList();

                    //Select only the lines which have an OrderLineStatus of Complete (948100005)
                    Int32 orderLinesWhereComplete = orderLinesByOrder.Where(a => a.GetAttributeValue<OptionSetValue>("fc_orderproductstatus").Value == 948100005).Count();

                    //if the count is greater than 1, update the Order Billing Status to "Ready To Bill" (salesorderid)
                    if (orderLinesWhereComplete > 0 && salesOrderId != Guid.Empty)
                    {
                        updateSuccess = updateOrderBillingStatus(salesOrderId, orderNo);
                        countUpdated++;
                    }
                    else
                    {
                        //outcomeLog.Info("OrderId : " + orderNo + " was not updated because no complete order lines were found");
                        CRMService.WriteLog("OrderId : " + orderNo + " was not updated because no complete order lines were found", true, true, localNLog);
                    }

                    countProcessed++;
                }

                outcomeLog.Info("Completed Operation One ----------------->" + countProcessed + " orders processed");
                outcomeLog.Info("Completed Operation One ----------------->" + countUpdated + " orders updated with new status Ready To Bill");
                CRMService.WriteLog("Completed Operation One ----------------->", true, true, localNLog);
            }
            else
            {
                CRMService.WriteLog("No OrderLines found to Process for Operation One", true, true, localNLog);
                outcomeLog.Info("No OrderLines found for Operation One");
            }
        }

        /// <summary>
        /// Operation Two : sets the Order Statuscode to either OrderLines Fulfilled or In Progress
        /// </summary>
        /// <param name="singleOrderNo">string representing the OrderNo input if available</param>
        static void executeOperationTwo(string singleOrderNo = null)
        {
            CRMService.WriteLog("Executing Operation Two ----------------->", true, true, localNLog);
            outcomeLog.Info("Executing Operation Two ----------------->");

            Int32[] billingStatuses = new Int32[] { (int)_billingStatus.ReadyToBill };
            List<Entity> foundOrderLines = null;
            if (singleOrderNo != null)
            {
                foundOrderLines = getOrderLines(billingStatuses, singleOrderNo);
            }
            else
            {
                foundOrderLines = getOrderLines(billingStatuses);
            }

            if (foundOrderLines.Count > 0)
            {
                CRMService.WriteLog("Found : " + foundOrderLines.Count + " OrderLines to process", true, true, localNLog);

                //Process Lines : 
                //Rule(s) :
                //1) if ALL orderlines are COMPLETE then the status reason should be OrderLines Fulfilled (948,150,001)
                //2) otherwise, the Status Reason is In Progress (3)

                //get a list of Orders (test 980005)
                var groupOrderLinesByOrder = foundOrderLines.GroupBy(item => (Guid)item.GetAttributeValue<AliasedValue>("so.salesorderid").Value).Select(group => new { OrderId = group.Key, Items = group.ToList() }).ToList();
                CRMService.WriteLog("Found : " + groupOrderLinesByOrder.Count() + " distinct Orders to process", true, true, localNLog);

                Int32 countProcessed = 0;
                foreach (var grpOrder in groupOrderLinesByOrder)
                {
                    bool updateSuccess = false; //set a false flag
                    string orderNo = "UNKNOWN";
                    if (grpOrder.Items[0].Attributes.Contains("so.name"))
                    {
                        orderNo = grpOrder.Items[0].GetAttributeValue<AliasedValue>("so.name").Value.ToString();
                    }
                    Guid salesOrderId = (Guid)grpOrder.OrderId;
                    //get the OrderLines only for this Order
                    List<Entity> orderLinesByOrder = foundOrderLines.Where(a => a.GetAttributeValue<AliasedValue>("so.name").Value.ToString() == orderNo).ToList();

                    //Select only the lines which have an OrderLineStatus of Complete (948100005) or Cancelled (948100006)
                    Int32 orderLinesWhereComplete = orderLinesByOrder.Where(a => a.GetAttributeValue<OptionSetValue>("fc_orderproductstatus").Value == 948100005).Count();
                    Int32 orderLinesWhereCancelled = orderLinesByOrder.Where(a => a.GetAttributeValue<OptionSetValue>("fc_orderproductstatus").Value == 948100006).Count();

                    //if the count is orderlines where Complete or Cancelled is the same as the OrderLines returned, then set the Order Statuscode
                    if (orderLinesWhereComplete + orderLinesWhereCancelled == orderLinesByOrder.Count())
                    {
                        updateSuccess = updateOrderStatuscode(salesOrderId, orderNo,948150001); // OrderLines Fulfilled
                    }
                    else
                    {
                        updateSuccess = updateOrderStatuscode(salesOrderId, orderNo, 3); // In Progress
                    }

                    countProcessed++;
                }

                outcomeLog.Info("Completed Operation Two ----------------->" + countProcessed + " orders processed");
                CRMService.WriteLog("Completed Operation Two ----------------->", true, true, localNLog);
            }
            else
            {
                CRMService.WriteLog("No OrderLines found to Process for Operation Two", true, true, localNLog);
                outcomeLog.Info("No OrderLines found for Operation Two");
            }
        }

        /// <summary>
        /// Select OrderLines by Query
        /// </summary>
        /// <param name="billingStatus">Lust of acceptable Billing Statuses to select by</param>
        /// <param name="orderNo">string representing the OrderNo input if available</param>
        /// <returns></returns>
        static List<Entity> getOrderLines(Int32[] billingStatus,string orderNo = null)
        {
            EntityCollection ret = null;
            List<Entity> allOrderLines = new List<Entity>();

            try
            {
                //select the Products for the Query
                Guid[] selectedProducts = selectProducts();

                if (selectedProducts.Length > 0)
                {
                    QueryExpression query = new QueryExpression("salesorderdetail");

                    // Add columns to query.ColumnSet
                    query.ColumnSet.AddColumns("productname", "fc_orderproductstatus", "fc_kaorderid", "productid");

                    // Define filter query.Criteria
                    query.Criteria.AddCondition(new ConditionExpression("productid", ConditionOperator.In, selectedProducts));

                    // Add link-entity so
                    LinkEntity so = query.AddLink("salesorder", "salesorderid", "salesorderid");
                    so.EntityAlias = "so";

                    // Add columns to so.Columns
                    so.Columns.AddColumns("fc_billingstatus", "statuscode", "salesorderid", "name");

                    // Define filter so.LinkCriteria
                    FilterExpression so_LinkCriteria_0 = new FilterExpression();
                    so.LinkCriteria.AddFilter(so_LinkCriteria_0);

                    // Define filter so_LinkCriteria_0
                    //so_LinkCriteria_0.FilterOperator = LogicalOperator.Or;
                    so_LinkCriteria_0.AddCondition(new ConditionExpression("fc_billingstatus", ConditionOperator.In, billingStatus));
                    //so_LinkCriteria_0.AddCondition(new ConditionExpression("fc_billingstatus", ConditionOperator.Equal, 948150001));

                    FilterExpression so_LinkCriteria_1 = new FilterExpression();
                    so.LinkCriteria.AddFilter(so_LinkCriteria_1);

                    // Define filter so_LinkCriteria_1
                    so_LinkCriteria_1.AddCondition("statuscode", ConditionOperator.NotEqual, (int)_orderStatus.OrderLinesFulfilled);

                    //Suppport for large data set
                    PagingInfo pageInfo = new PagingInfo();
                    pageInfo.Count = 5000;
                    pageInfo.PageNumber = 1;
                    query.PageInfo = pageInfo;

                    if (orderNo != null)
                    {
                        //if we only want to get a orderlines for a single Order
                        FilterExpression so_LinkCriteria_2 = new FilterExpression();
                        so.LinkCriteria.AddFilter(so_LinkCriteria_2);

                        // Define filter so_LinkCriteria_2
                        so_LinkCriteria_2.AddCondition("name", ConditionOperator.Equal, orderNo);

                    }

                    
                    while (true)
                    {
                        ret = _service.RetrieveMultiple(query);

                        if (ret.Entities != null)
                        {
                            CRMService.WriteLog("Found " + ret.Entities.Count + " OrderLines to process",true,true,localNLog);
                            if (ret.Entities.Count > 0)
                            {
                                CRMService.WriteLog("Adding " + ret.Entities.Count + " to total count : " + allOrderLines.Count,true,true,localNLog);
                                CRMService.WriteLog("Fetching Page Number : " + query.PageInfo.PageNumber,true,true,localNLog);
                                foreach (Entity e in ret.Entities)
                                {
                                    allOrderLines.Add(e);
                                }
                            }
                        }

                        if (ret.MoreRecords)
                        {
                            CRMService.WriteLog("Large Query : Page Number : " + query.PageInfo.PageNumber,true,true,localNLog);
                            query.PageInfo.PageNumber++;
                            query.PageInfo.PagingCookie = ret.PagingCookie;
                        }
                        else
                        {
                            // If no more records in the loop, exit
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CRMService.WriteLog("Error in IssueStatus.getOrderLines : " + ex.Message,true,true,localNLog);
            }
            

            return allOrderLines;
        }

        /// <summary>
        /// Update the BillingStatus of the Order
        /// </summary>
        /// <param name="orderId">Guid/param>
        /// <param name="orderNo">String</param>
        /// <returns></returns>
        static bool updateOrderBillingStatus(Guid orderId,string orderNo)
        {
            //fc_billingstatus
            bool ret = false;
            try
            {
                if (orderId != Guid.Empty)
                {
                    //retrieve the entity to be updated
                    Entity orderToUpdate = _crmOperations.retrieveEntity("salesorder", orderId, new ColumnSet(new string[] { "name","fc_billingstatus" }));

                    if (orderToUpdate != null)
                    {
                        //Set new Order (billing status) to be updated
                        orderToUpdate["fc_billingstatus"] = new OptionSetValue(948150001); //Ready To Bill

                        bool orderUpdated = _crmOperations.updateEntity(orderToUpdate);

                        if (orderUpdated)
                        {
                            CRMService.WriteLog("OrderId : " + orderNo + " updated to Ready To Bill", true, true, localNLog);
                            outcomeLog.Info("OrderId : " + orderNo + " updated to Ready To Bill");
                            ret = true;
                        }
                        else
                        {
                            //could not update for some reason
                            CRMService.WriteLog("OrderId : " + orderNo + "could NOT be updated", true, true, localNLog);
                            //outcomeLog.Info("OrderId : " + orderNo + " could NOT be updated");
                        }
                    }
                }
                else
                {
                    CRMService.WriteLog("Error : IssueStatus.updateOrderBillingStatus : OrderId is missing!", true, true, localNLog);
                }
            }
            catch (Exception ex)
            {
                CRMService.WriteLog("Error in IssueStatus.updateOrderBillingStatus (OrderId : " + orderId+ ") : " + ex.Message, true, true, localNLog);
            }
            return ret;
        }

        /// <summary>
        /// Updates the Order StatusCode value
        /// </summary>
        /// <param name="orderId">Guid</param>
        /// <param name="orderNo">string</param>
        /// <param name="statuscode">Int32</param>
        /// <returns></returns>
        static bool updateOrderStatuscode(Guid orderId, string orderNo,Int32 statuscode)
        {
            bool ret = false;
            try
            {
                string statuscodename = string.Empty;
                if (statuscode == 3)
                {
                    statuscodename = "In Progress";
                }
                else
                {
                    statuscodename = "OrderLines Fulfilled";
                }

                if (orderId != Guid.Empty)
                {
                    //retrieve the entity to be updated
                    Entity orderToUpdate = _crmOperations.retrieveEntity("salesorder", orderId, new ColumnSet(new string[] { "name", "statuscode", "statecode" }));

                    if (orderToUpdate != null)
                    {
                        

                        bool orderUpdated = _crmOperations.setStateRequest("salesorder",orderId,1,statuscode);

                        if (orderUpdated)
                        {
                            CRMService.WriteLog("OrderId : " + orderNo + " updated to "+statuscodename, true, true, localNLog);
                            outcomeLog.Info("OrderId : " + orderNo + " updated to "+statuscodename);
                            ret = true;
                        }
                        else
                        {
                            //could not update for some reason
                            CRMService.WriteLog("OrderId : " + orderNo + " could NOT be updated", true, true, localNLog);
                            //outcomeLog.Info("OrderId : " + orderNo + " could NOT be updated");
                        }
                    }
                }
                else
                {
                    CRMService.WriteLog("Error : IssueStatus.updateOrderStatuscode : OrderId is missing!", true, true, localNLog);
                }

            }
            catch (Exception ex)
            {
                CRMService.WriteLog("Error in IssueStatus.updateOrderStatuscode (OrderId : " + orderId + ") : " + ex.Message, true, true, localNLog);
            }

            return ret;
        }

        /// <summary>
        /// Returns the GUIDs for the applicable Products in the Query for each environment
        /// </summary>
        /// <returns>Guid[] array</returns>
        private static Guid[] selectProducts()
        {
            Guid[] ret = null;

            //EPC, FloorPlan, Photography, 360Photography, Trailer
            //Note the GUIDs are all the same but it may be possible in the future to have different Products in different environments
            switch (_connType)
            {
                case "Dev":
                    ret = new Guid[] { new Guid("E407A4FD-4C11-EA11-A811-00224801C499"), new Guid("A668B6DA-4C11-EA11-A811-00224801C499"), new Guid("EA3E5AAF-4C11-EA11-A811-00224801C499"), new Guid("B4329665-0312-EA11-A811-00224801CECD"), new Guid("47CC8C92-4C11-EA11-A811-00224801C499")};
                    break;
                case "Stage":
                    ret = new Guid[] { new Guid("E407A4FD-4C11-EA11-A811-00224801C499"), new Guid("A668B6DA-4C11-EA11-A811-00224801C499"), new Guid("EA3E5AAF-4C11-EA11-A811-00224801C499"), new Guid("B4329665-0312-EA11-A811-00224801CECD"), new Guid("47CC8C92-4C11-EA11-A811-00224801C499") };
                    break;
                case "Prod":
                    ret = new Guid[] { new Guid("E407A4FD-4C11-EA11-A811-00224801C499"), new Guid("A668B6DA-4C11-EA11-A811-00224801C499"), new Guid("EA3E5AAF-4C11-EA11-A811-00224801C499"), new Guid("B4329665-0312-EA11-A811-00224801CECD"), new Guid("47CC8C92-4C11-EA11-A811-00224801C499") };
                    break;
            }

            return ret;
        }

        /// <summary>
        /// Helper to validate OrderNo input of the parameters as digits
        /// </summary>
        /// <param name="s">string</param>
        /// <returns>boolean</returns>
        static bool isAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (!char.IsDigit(c))
                    return false;
            }
            return true;
        }
    }
}
