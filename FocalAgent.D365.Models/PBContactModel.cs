﻿using System;

namespace FocalAgent.D365.Models
{
    public class AAContactModel
    {
        public string contactId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string parentCustomerId { get; set; }
        public string suffix { get; set; }
        public string aaCustomerId { get; set; }
        public string name { get; set; }
        public string businessTypeCode { get; set; }
    }

    public class CRMContactModel
    {
        public Guid contactId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobilePhone { get; set; }
        public string businessPhone { get; set; }
        public string emailAddress { get; set; }
        public Guid parentCustomerId { get; set; }

        public Guid newAccountId { get; set; }
        public string AAId { get; set; }

        public CRMAddressModel crmAddress { get; set; }

    }

    public class CRMAddressModel
    {
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string Address_3 { get; set; }
        public string Address_City { get; set; }
        public string Address_County { get; set; }
        public string Address_Postcode { get; set; }

    }
}
